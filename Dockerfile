FROM artefact.skao.int/ska-build-python:0.1.1 as build

WORKDIR /code

COPY pyproject.toml poetry.lock* ./

ENV POETRY_NO_INTERACTION=1
ENV POETRY_VIRTUALENVS_IN_PROJECT=1
ENV POETRY_VIRTUALENVS_CREATE=1

#no-root is required because in the build
#step we only want to install dependencies
#not the code under development
RUN poetry install --no-root

FROM artefact.skao.int/ska-python:0.1.2

#Adding the virtualenv binaries
#to the PATH so there is no need
#to activate the venv
ENV VIRTUAL_ENV=/code/.venv
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

COPY --from=build ${VIRTUAL_ENV} ${VIRTUAL_ENV}

WORKDIR /code

COPY ./src/ /code/  

CMD ["uvicorn", "ska_tango_ping.main:app", "--proxy-headers", "--host", "0.0.0.0", "--port", "8000", "--log-level", "debug", "--ssl-keyfile", "/certs/tls.key", "--ssl-certfile", "/certs/tls.crt"]