# CAR_OCI_REGISTRY_HOST and PROJECT are combined to define the Docker tag 
# for this project. The definition below inherits the standard value for 
#
PROJECT = ska-tango-ping

# KUBE_NAMESPACE defines the Kubernetes Namespace that will be deployed to
# using Helm.  If this does not already exist it will be created
KUBE_NAMESPACE ?= ska-tango-ping

# UMBRELLA_CHART_PATH Path of the umbrella chart to work with
HELM_CHART ?= test-parent
K8S_UMBRELLA_CHART_PATH ?= charts/$(HELM_CHART)/
K8S_CHART = $(HELM_CHART)
K8S_CHARTS = $(K8S_CHART)
HELM_RELEASE = $(HELM_CHART)

TEST_RUNNER ?= runner-$(CI_JOB_ID)-$(RELEASE_NAME)##name of the pod running the k8s_tests

CI_REGISTRY ?= gitlab.com

DOTENV_PATH ?= "src/ska_tango_ping/.env"
# ST-1169: Whether the deployment is targetting minikube or not
MINIKUBE ?= true
VAULT_ENABLED ?= false
NAMESPACE_PREFIX ?= true

CLUSTER_DOMAIN ?= cluster.local## Domain used for naming Tango Device Servers

SVC_NAME ?= ska-tango-ping## name of the service

API_KEY_SECRET ?= fakesecret

CERT_DIR = /tmp/ska-tango-ping/certs


# Run from local image only, requires either a pulled or local image 
# always run "latest" by default in dev environment
CUSTOM_VALUES ?= 

ifneq ($(CI_JOB_ID),)
CUSTOM_VALUES = --set ska-tango-ping.image.repository=$(CI_REGISTRY)/ska-telescope/$(PROJECT)/$(PROJECT) \
	--set ska-tango-ping.image.tag=$(VERSION)-dev.c$(CI_COMMIT_SHORT_SHA)
K8S_TEST_IMAGE_TO_TEST=$(CI_REGISTRY)/ska-telescope/$(PROJECT)/$(PROJECT):$(VERSION)-dev.c$(CI_COMMIT_SHORT_SHA)
else
CUSTOM_VALUES = --set ska-tango-ping.image.repository=$(CAR_OCI_REGISTRY_HOST)/$(PROJECT) \
	--set ska-tango-ping.image.tag=$(VERSION)
K8S_TEST_IMAGE_TO_TEST = $(CAR_OCI_REGISTRY_HOST)/$(PROJECT):$(VERSION)
endif

# In production environment get docker image from Nexus (not GitLab)
ENV_CHECK := $(shell echo $(CI_ENVIRONMENT_SLUG) | egrep production)
ifneq ($(ENV_CHECK),)
CUSTOM_VALUES = 
endif

HELM_RELEASE = ska-tango-ping

#
# include makefile to pick up the standard Make targets, e.g., 'make build'
# build, 'make push' docker push procedure, etc. The other Make targets
# ('make interactive', 'make test', etc.) are defined in this file.
#
include .make/oci.mk
include .make/k8s.mk
include .make/python.mk
include .make/helm.mk
include .make/base.mk

# define private overrides for above variables in here
-include PrivateRules.mak

K8S_CHART_PARAMS = --set global.minikube=$(MINIKUBE) \
	--set global.cluster_domain=$(CLUSTER_DOMAIN) \
	--set ska-tango-ping.ingress.namespacePrefix=$(NAMESPACE_PREFIX) \
	--set ska-tango-ping.apiKeySecret.secretProviderEnabled=$(VAULT_ENABLED) \
	$(CUSTOM_VALUES)

PYTHON_VARS_AFTER_PYTEST = -m 'not post_deployment' --disable-pytest-warnings
#
# IMAGE_TO_TEST defines the tag of the Docker image to test
#
ifneq ($(CI_JOB_ID),)
IMAGE_TO_TEST=$(CI_REGISTRY)/ska-telescope/$(PROJECT):$(VERSION)
else
IMAGE_TO_TEST = $(CAR_OCI_REGISTRY_HOST)/$(PROJECT):$(VERSION)
endif

PYTHON_VARS_BEFORE_PYTEST = PYTHONPATH=/app/src:src SVC_NAME=$(SVC_NAME):8000 KUBE_NAMESPACE=$(KUBE_NAMESPACE) API_KEY=$(API_KEY_SECRET)

ifeq ($(strip $(firstword $(MAKECMDGOALS))),k8s-test)
PYTHON_VARS_AFTER_PYTEST := -m 'post_deployment' --disable-pytest-warnings
endif

ifneq ($(CI_JOB_ID),)
NETWORK_MODE := ska-tango-ping-$(CI_JOB_ID)
CONTAINER_NAME_PREFIX := $(PROJECT)-$(CI_JOB_ID)-
else
CONTAINER_NAME_PREFIX := $(PROJECT)-
endif

vars: ## Print out defined variables
	$(foreach v, $(.VARIABLES), $(if $(filter file,$(origin $(v))), $(info $(v)=$($(v)))))

getcerts:
	rm -rf $(CERT_DIR)
	mkdir -p $(CERT_DIR)
	openssl req -x509 -newkey rsa:2048 -keyout $(CERT_DIR)/tls.key -out $(CERT_DIR)/tls.crt -days 365 -nodes -subj "/CN=$(SVC_NAME).$(KUBE_NAMESPACE).svc"
	cp $(CERT_DIR)/tls.crt $(CERT_DIR)/tls.key build/

certs:
	# from https://kubernetes.github.io/ingress-nginx/deploy/validating-webhook/
	rm -rf $(CERT_DIR)
	mkdir -p $(CERT_DIR)
	@echo "[req]" >> $(CERT_DIR)/csr.conf && \
	echo "req_extensions = v3_req" >> $(CERT_DIR)/csr.conf && \
	echo "distinguished_name = req_distinguished_name" >> $(CERT_DIR)/csr.conf && \
	echo "[req_distinguished_name]" >> $(CERT_DIR)/csr.conf && \
	echo "[ v3_req ]" >> $(CERT_DIR)/csr.conf && \
	echo "basicConstraints = CA:FALSE" >> $(CERT_DIR)/csr.conf && \
	echo "keyUsage = nonRepudiation, digitalSignature, keyEncipherment" >> $(CERT_DIR)/csr.conf && \
	echo "extendedKeyUsage = serverAuth" >> $(CERT_DIR)/csr.conf && \
	echo "subjectAltName = @alt_names" >> $(CERT_DIR)/csr.conf && \
	echo "[alt_names]" >> $(CERT_DIR)/csr.conf && \
	echo "DNS.1 = $(SVC_NAME)" >> $(CERT_DIR)/csr.conf && \
	echo "DNS.2 = $(SVC_NAME).$(KUBE_NAMESPACE)" >> $(CERT_DIR)/csr.conf && \
	echo "DNS.3 = $(SVC_NAME).$(KUBE_NAMESPACE).svc" >> $(CERT_DIR)/csr.conf && \
	cat $(CERT_DIR)/csr.conf
	openssl genrsa -out $(CERT_DIR)/tls.key 2048
	openssl req -new -key $(CERT_DIR)/tls.key \
	-subj "/CN=$(SVC_NAME).$(KUBE_NAMESPACE).svc" \
	-out $(CERT_DIR)/server.csr \
	-config $(CERT_DIR)/csr.conf
	@echo "apiVersion: certificates.k8s.io/v1" >> $(CERT_DIR)/approve.yaml && \
	echo "kind: CertificateSigningRequest" >> $(CERT_DIR)/approve.yaml && \
	echo "metadata:">>$(CERT_DIR)/approve.yaml && \
	echo "  name: $(SVC_NAME).$(KUBE_NAMESPACE).svc" >> $(CERT_DIR)/approve.yaml && \
	echo "spec:">>$(CERT_DIR)/approve.yaml && \
	echo "  signerName: kubernetes.io/kube-apiserver-client">>$(CERT_DIR)/approve.yaml && \
	echo "  request: $$(cat $(CERT_DIR)/server.csr | base64 | tr -d '\n')">>$(CERT_DIR)/approve.yaml && \
	echo "  usages:">>$(CERT_DIR)/approve.yaml && \
	echo "  - digital signature">>$(CERT_DIR)/approve.yaml && \
	echo "  - key encipherment">>$(CERT_DIR)/approve.yaml && \
	echo "  - server auth">>$(CERT_DIR)/approve.yaml
	ls -latr $(CERT_DIR)
	cat $(CERT_DIR)/approve.yaml
	kubectl delete -f $(CERT_DIR)/approve.yaml || true
	kubectl apply -f $(CERT_DIR)/approve.yaml
	sleep 3
	kubectl certificate approve $(SVC_NAME).$(KUBE_NAMESPACE).svc

secret: k8s-namespace
	kubectl delete secret server-cert -n $(KUBE_NAMESPACE) || true
	kubectl create secret generic server-cert \
	--from-file=tls.key=$(CERT_DIR)/tls.key \
	--from-file=tls.crt=$(CERT_DIR)/tls.crt \
	-n $(KUBE_NAMESPACE)

.PHONY: vars
