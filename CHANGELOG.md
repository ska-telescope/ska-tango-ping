# Changelog

All notable changes to this project will be documented in this file.
This project adheres to `Semantic Versioning <http://semver.org/>`_.

## Version 0.1.7

Base image adoption.

* Updated poetry files, charts version and oci image Dockerfile in order to increase the security problems. 
* Adopted SKA base image

