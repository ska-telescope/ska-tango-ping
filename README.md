# SKA TANGO Ping Service

This project is a fastAPI application that allow to check if a device respond correctly to the State attribute call.

It is intended as a scalable ping service to be used by the TANGO operator to check if a device is working without using the c++ tango_admin tool. 

## Structure

Project structure is following a basic python file structure for [FastAPI](https://fastapi.tiangolo.com/) as below:

```console
.
├── charts
│   └── ...
├── docs
│   └── ...
├── src
│   └── ...
└── tests
│   └── ...
├── Dockerfile
├── LICENSE
├── Makefile
├── pyproject.toml
└── README.md

```

Basically, this project uses the following technologies:

- Docker: To build a docker image that exposes port _80_ for API endpoints.
- Kubernetes and Helm: Project also includes a helm chart to deploy the above image in a loadbalanced kubernetes cluster.
- PyTango: to ping devices. 


## How to use it


## How to Use

Clone this repo: 
```
$ git clone https://gitlab.com/ska-telescope/ska-tango-ping.git
$ cd ska-tango-ping
$ git submodule update --init --recursive
```

Build a new Docker image for the project:
```
$ make oci-build
[...]
[+] Building 111.7s (14/14) FINISHED 
[...]
```

Install python requirements:
```
poetry install
```

Run python-test:
```
$ poetry shell
$ $ make python-test
.make/oci.mk:35: Setting CAR_OCI_REGISTRY_HOST to `harbor.skao.int/staging`
pytest 7.4.4
SVC_NAME=ska-tango-ping:8000 KUBE_NAMESPACE=ska-tango-ping API_KEY=fakesecret  pytest -m 'not post_deployment' --disable-pytest-warnings \
 --cov=src --cov-report=term-missing --cov-report html:build/reports/code-coverage --cov-report xml:build/reports/code-coverage.xml --junitxml=build/reports/unit-tests.xml tests/
=============================================================================================== test session starts ================================================================================================
platform linux -- Python 3.10.12, pytest-7.4.4, pluggy-1.4.0 -- /home/ubuntu/.cache/pypoetry/virtualenvs/ska-tango-ping-mAg9g0jX-py3.10/bin/python
cachedir: .pytest_cache
metadata: {'Python': '3.10.12', 'Platform': 'Linux-5.15.146.1-microsoft-standard-WSL2-x86_64-with-glibc2.35', 'Packages': {'pytest': '7.4.4', 'pluggy': '1.4.0'}, 'Plugins': {'bdd': '4.1.0', 'cov': '3.0.0', 'json-report': '1.5.0', 'anyio': '4.3.0', 'metadata': '3.1.1'}}
rootdir: /home/ubuntu/ska-tango-ping/tests
configfile: pytest.ini
plugins: bdd-4.1.0, cov-3.0.0, json-report-1.5.0, anyio-4.3.0, metadata-3.1.1
collected 14 items / 5 deselected / 9 selected                                                                                                                                                                     

tests/unit/test_main.py::test_init 
-------------------------------------------------------------------------------------------------- live log call ---------------------------------------------------------------------------------------------------
1|2024-04-15T15:16:22.150Z|INFO|MainThread|_send_single_request|_client.py#1026||HTTP Request: GET http://testserver/ "HTTP/1.1 200 OK"
PASSED                                                                                                                                                                                                       [ 11%]
----------------------------------------------------------------------- generated json file: /home/ubuntu/ska-tango-ping/build/cucumber.json -----------------------------------------------------------------------
------------------------------------------------------------------- generated xml file: /home/ubuntu/ska-tango-ping/build/reports/unit-tests.xml -------------------------------------------------------------------
--------------------------------------------------------------------------------------------------- JSON report ----------------------------------------------------------------------------------------------------
report saved to: build/reports/report.json

---------- coverage: platform linux, python 3.10.12-final-0 ----------
Name                         Stmts   Miss  Cover   Missing
----------------------------------------------------------
src/ska_tango_ping/main.py     103      7    93%   177-187
----------------------------------------------------------
TOTAL                          103      7    93%
Coverage HTML written to dir build/reports/code-coverage
Coverage XML written to file build/reports/code-coverage.xml

============================================================================== 8 passed, 5 deselected, 1 xfailed, 6 warnings in 0.91s ==============================================================================
```

Python linting:
```
$ make python-lint
[...]
--------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
```

Helm Charts linting:
```
$ make helm-lint
[...]
10 chart(s) linted, 0 chart(s) failed
```

In order to install the chart, first create the server certificates for the web application: 
```
$ make certs && make getcerts && make secret 
```


After that, install it with
```
$ make k8s-install-chart
[...]
NAME: test
LAST DEPLOYED: Tue Jun  8 22:37:03 2021
NAMESPACE: ska-tango-ping
STATUS: deployed
REVISION: 1
TEST SUITE: None
```

Test the deployment with
```
$ make k8s-test
```

Uninstall the chart: 
```
$ make k8s-uninstall-chart 
release "test" uninstalled
```
