# pylint: disable=redefined-outer-name, unused-argument
import datetime
import json
import logging
import os

import dsconfig.dump
import dsconfig.json2tango
import tango
from fastapi import FastAPI, HTTPException, Request, Security, status
from fastapi.responses import JSONResponse
from fastapi.security import APIKeyHeader

TIMEOUT_MILLIS = int(os.environ.get("TIMEOUT_MILLIS", 500))

app = FastAPI()

logger = logging.getLogger("uvicorn")

api_key_header = APIKeyHeader(name="x-api-key", auto_error=False)


def get_api_key(
    api_key_header: str = Security(api_key_header),
) -> str:
    """Get and validate an API key from the query parameters or HTTP header.

    Args:
        api_key_header: The API key passed in the HTTP header.

    Returns:
        The validated API key.

    Raises:
        HTTPException: If the API key is invalid or missing.
    """
    if api_key_header in os.environ["API_KEY"]:
        return api_key_header
    raise HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Invalid or missing API Key",
    )


@app.get("/")
def read_root():
    return {"Hello": "SKA TANGO Ping Service"}


@app.get("/isdatabasedsready/{tango_host}/{port}")
def is_databaseds_ready(tango_host: str, port: str):
    try:
        db = tango.Database(tango_host, port)
        list = db.get_device_exported("*")
        content = {"exported_size": list.size()}
        return JSONResponse(content=content, status_code=status.HTTP_200_OK)
    except Exception as ex:
        logger.error(str(ex), exc_info=ex)
        content = {"Exception": str(ex)}
        return JSONResponse(
            content=content, status_code=status.HTTP_417_EXPECTATION_FAILED
        )


@app.get("/ping/{tango_host}/{domain}/{family}/{member}")
def device_ping(
    tango_host: str,
    domain: str,
    family: str,
    member: str,
    timeout: int = TIMEOUT_MILLIS,
):
    try:
        device_fqdn = (
            f"tango://{tango_host}/{domain}/{family}/{member}"  # noqa
        )
        dev = tango.DeviceProxy(device_fqdn)
        dev.set_timeout_millis(timeout)
        ping = dev.ping()
        content = {"ping": ping}
        return JSONResponse(content=content, status_code=status.HTTP_200_OK)
    except Exception as ex:
        logger.error(str(ex), exc_info=ex)
        content = {"Exception": str(ex)}
        return JSONResponse(
            content=content, status_code=status.HTTP_417_EXPECTATION_FAILED
        )


@app.get("/state/{tango_host}/{domain}/{family}/{member}")
def device_state(
    tango_host: str,
    domain: str,
    family: str,
    member: str,
    timeout: int = TIMEOUT_MILLIS,
):
    try:
        device_fqdn = (
            f"tango://{tango_host}/{domain}/{family}/{member}"  # noqa
        )
        dev = tango.DeviceProxy(device_fqdn)
        dev.set_timeout_millis(timeout)
        dev_state = dev.State()
        content = {"state": str(dev_state)}
        return JSONResponse(content=content, status_code=status.HTTP_200_OK)
    except Exception as ex:
        logger.error(str(ex), exc_info=ex)
        content = {"Exception": str(ex)}
        return JSONResponse(
            content=content, status_code=status.HTTP_417_EXPECTATION_FAILED
        )


@app.get("/isconfigured/{tango_host}/{domain}/{family}/{member}")
def is_device_configured(
    tango_host: str, domain: str, family: str, member: str
):
    try:
        device_fqdn = (
            f"tango://{tango_host}/{domain}/{family}/{member}"  # noqa
        )
        tango.DeviceProxy(device_fqdn)
        content = {"configured": "true"}
        return JSONResponse(content=content, status_code=status.HTTP_200_OK)
    except Exception as ex:
        logger.error(str(ex), exc_info=ex)
        content = {"Exception": str(ex)}
        return JSONResponse(
            content=content, status_code=status.HTTP_417_EXPECTATION_FAILED
        )


@app.get("/config/{tango_host}/{port}/{server}/{instance}")
def get_config(tango_host: str, port: int, server: str, instance: str):
    try:
        db = tango.Database(tango_host, port)
        dbdata = dsconfig.dump.get_db_data(
            db,
            patterns=[f"server:{server}/{instance}"],
            properties=True,
            class_properties=True,
            attribute_properties=True,
            aliases=True,
            dservers=False,
            subdevices=True,
        )
        content = json.dumps(dbdata, ensure_ascii=False, sort_keys=True)
        logger.info(content)
        return JSONResponse(
            content=content,
            status_code=status.HTTP_200_OK,
        )
    except Exception as ex:
        logger.error(str(ex), exc_info=ex)
        content = {"Exception": str(ex)}
        return JSONResponse(
            content=content, status_code=status.HTTP_417_EXPECTATION_FAILED
        )


@app.get("/states/{tango_host}/{port}/{server}/{instance}")
def device_states(
    tango_host: str,
    port: int,
    server: str,
    instance: str,
    timeout: int = TIMEOUT_MILLIS,
):
    try:
        db = tango.Database(tango_host, port)
        dbdata = dsconfig.dump.get_db_data(
            db,
            patterns=[f"server:{server}/{instance}"],
            properties=False,
            class_properties=False,
            attribute_properties=False,
            aliases=False,
            dservers=False,
            subdevices=True,
        )
        dev_count = 0
        dev_ready_count = 0
        devices = []
        servers = dbdata["servers"]
        for server in servers:
            for instance in servers[server]:
                for cls in servers[server][instance]:
                    for dev in servers[server][instance][cls]:
                        dev_count += 1
                        device_fqdn = (
                            f"tango://{tango_host}:{port}/{dev}"  # noqa
                        )
                        dev_proxy = tango.DeviceProxy(device_fqdn)
                        dev_proxy.set_timeout_millis(timeout)
                        try:
                            dev_state = dev_proxy.State()
                            dev_ready_count += 1
                        except Exception as ex:
                            logger.error(str(ex), exc_info=ex)
                            dev_state = "*UNAVAILABLE*"

                        try:
                            dev_status = dev_proxy.Status()
                        except Exception as ex:
                            logger.error(str(ex), exc_info=ex)
                            dev_status = "*UNAVAILABLE*"

                        try:
                            dev_ping = dev_proxy.ping()
                        except Exception as ex:
                            logger.error(str(ex), exc_info=ex)
                            dev_ping = -1

                        content = {
                            "name": dev,
                            "ping": dev_ping,
                            "state": str(dev_state),
                            "status": dev_status,
                            "class": cls,
                            "rettime": datetime.datetime.now(
                                datetime.timezone.utc
                            ).strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
                        }  # 2006-01-02T15:04:05Z07:00
                        devices.append(content)
        result = {
            "dev_count": dev_count,
            "dev_ready_count": dev_ready_count,
            "devices": devices,
        }
        content = json.dumps(result, ensure_ascii=False, sort_keys=True)
        logger.info("%s/%s:%s", server, instance, content)
        return JSONResponse(
            content=content,
            status_code=status.HTTP_200_OK,
        )
    except Exception as ex:
        logger.error(str(ex), exc_info=ex)
        content = {"Exception": str(ex)}
        return JSONResponse(
            content=content, status_code=status.HTTP_417_EXPECTATION_FAILED
        )


@app.post("/configuredb")
async def configure_db(request: Request, api_key: str = Security(get_api_key)):
    step = 0
    postJson = None
    try:
        postJson = await request.json()
        step += 1
        config = json.loads(postJson["config_content"])
        step += 1
        tango_host = postJson["tango_host"]
        step += 1
        port = postJson["port"]
        step += 1
        db = tango.Database(tango_host, port)
        step += 1
        config = dsconfig.json2tango.prepare_config(config)
        step += 1
        results = dsconfig.json2tango.apply_config(
            config,
            db,
            write=True,
            update=True,
            original=None,
            nostrictcheck=False,
            case_sensitive=False,
            cleanup_protected_props=False,
            sleep=0.01,
            verbose=False,
        )
        step += 1
        retval = dsconfig.json2tango.show_output(
            *results,
            show_colors=False,
            show_input=False,
            show_output=False,
            show_dbcalls=False,
            verbose=False,
            write=True,
            show_json=False,
        )
        step += 1
        content = {"Result": retval, "step": step, "config": config}
        if retval == 1:
            return JSONResponse(
                content=content, status_code=status.HTTP_405_METHOD_NOT_ALLOWED
            )
        if retval == 3:
            return JSONResponse(
                content=content, status_code=status.HTTP_400_BAD_REQUEST
            )
        return JSONResponse(content=content, status_code=status.HTTP_200_OK)
    except Exception as ex:
        logger.error(str(ex), exc_info=ex)
        content = {
            "Exception": str(ex),
            "step": str(step),
            "postJson": postJson,
        }
        return JSONResponse(
            content=content, status_code=status.HTTP_417_EXPECTATION_FAILED
        )
