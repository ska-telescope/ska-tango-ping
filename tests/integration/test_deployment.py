import json
import os

import pytest
import requests


@pytest.mark.post_deployment
def test_ping_tangotest():
    SVC_NAME = os.getenv("SVC_NAME")
    namespace = os.getenv("KUBE_NAMESPACE")
    url = f"https://{SVC_NAME}/ping/databaseds-tango-base-test.{namespace}:10000/test/power_supply/1?timeout=50"  # noqa
    response = requests.get(url, timeout=1, verify=False)
    assert response.status_code == 200


@pytest.mark.post_deployment
def test_state_tangotest():
    SVC_NAME = os.getenv("SVC_NAME")
    namespace = os.getenv("KUBE_NAMESPACE")
    url = f"https://{SVC_NAME}/state/databaseds-tango-base-test.{namespace}:10000/test/power_supply/1?timeout=50"  # noqa
    response = requests.get(url, timeout=1, verify=False)
    assert response.status_code == 200
    assert response.json()["state"] == "STANDBY"


@pytest.mark.post_deployment
def test_configured_tangotest():
    SVC_NAME = os.getenv("SVC_NAME")
    namespace = os.getenv("KUBE_NAMESPACE")
    url = f"https://{SVC_NAME}/isconfigured/databaseds-tango-base-test.{namespace}:10000/test/power_supply/1"  # noqa
    response = requests.get(url, timeout=1, verify=False)
    assert response.status_code == 200


@pytest.mark.post_deployment
def test_get_config():
    SVC_NAME = os.getenv("SVC_NAME")
    namespace = os.getenv("KUBE_NAMESPACE")
    url = f"https://{SVC_NAME}/config/databaseds-tango-base-test.{namespace}/10000/PowerSupply/*"  # noqa
    response = requests.get(url, timeout=1, verify=False)
    servers = json.loads(response.json())["servers"]
    for server in servers:
        for instance in servers[server]:
            for cls in servers[server][instance]:
                for dev in servers[server][instance][cls]:
                    assert dev in [
                        "test/power_supply/1",
                        "test/power_supply/2",
                    ]
    assert response.status_code == 200


@pytest.mark.post_deployment
def test_states():
    SVC_NAME = os.getenv("SVC_NAME")
    namespace = os.getenv("KUBE_NAMESPACE")
    url = f"https://{SVC_NAME}/states/databaseds-tango-base-test.{namespace}/10000/PowerSupply/*"  # noqa
    response = requests.get(url, timeout=1, verify=False)
    devices = json.loads(response.json())
    assert devices["dev_count"] == 2
    assert devices["dev_ready_count"] == 2
    assert devices["devices"][0]["name"] == "test/power_supply/1"
    assert devices["devices"][0]["state"] == "STANDBY"
    assert devices["devices"][1]["name"] == "test/power_supply/2"
    assert devices["devices"][1]["state"] == "STANDBY"
    assert response.status_code == 200


@pytest.mark.post_deployment
def test_isdatabaseds_ready():
    SVC_NAME = os.getenv("SVC_NAME")
    namespace = os.getenv("KUBE_NAMESPACE")
    url = f"https://{SVC_NAME}/isdatabasedsready/databaseds-tango-base-test.{namespace}/10000"  # noqa
    response = requests.get(url, timeout=1, verify=False)
    assert response.status_code == 200


@pytest.mark.post_deployment
def test_configuredb():
    SVC_NAME = os.getenv("SVC_NAME")
    namespace = os.getenv("KUBE_NAMESPACE")
    apiKey = os.getenv("API_KEY")
    url = f"https://{SVC_NAME}/configuredb"  # noqa
    config_content = {
        "servers": {
            "timer": {
                "counters": {
                    "Counter": {
                        "test/counter/minutes": {},
                        "test/counter/seconds": {},
                    }
                }
            }
        }
    }
    myobj = {
        "tango_host": f"databaseds-tango-base-test.{namespace}",
        "port": "10000",
        "config_content": json.dumps(config_content),
    }
    response = requests.post(
        url, json=myobj, timeout=1, headers={"x-api-key": apiKey}, verify=False
    )
    assert response.status_code == 200
