import json
import os
from unittest.mock import Mock, patch

from fastapi.testclient import TestClient

from ska_tango_ping.main import app

test_client = TestClient(app)

apiKey = os.environ.get("API_KEY")


def test_init():
    response = test_client.get("/")
    assert response.status_code == 200
    assert response.json() == {"Hello": "SKA TANGO Ping Service"}


@patch("tango.DeviceProxy")
def test_ping_tangotest(proxy):
    url = "/ping/databaseds-tango-base-test:10000/test/tg_test/1?timeout=50"  # noqa
    response = test_client.get(url)
    assert response.status_code == 417
    proxy.return_value.ping.return_value = 1
    response = test_client.get(url)
    assert response.status_code == 200


@patch("tango.DeviceProxy")
def test_state_tangotest(proxy):
    url = "/state/databaseds-tango-base-test:10000/test/tg_test/1?timeout=50"  # noqa
    proxy.return_value.State.return_value = "STANDBY"
    response = test_client.get(url)
    assert response.status_code == 200
    assert response.json()["state"] == "STANDBY"


def test_notconfigured_tangotest():
    url = "/isconfigured/databaseds-tango-base-test:10000/test/tg_test/1?timeout=50"  # noqa
    response = test_client.get(url)
    assert response.status_code == 417


@patch("tango.DeviceProxy")
def test_configured_tangotest(proxy):
    # pylint: disable=W0613
    url = "/isconfigured/databaseds-tango-base-test:10000/test/tg_test/1?timeout=50"  # noqa
    response = test_client.get(url)
    assert response.status_code == 200


class Exported(Mock):
    def size(self):
        return 1


@patch("tango.Database")
def test_isdatabaseds_ready(proxy):
    url = "/isdatabasedsready/databaseds-tango-base-test/10000"  # noqa
    response = test_client.get(url)
    assert response.status_code == 417
    proxy.return_value.get_device_exported = Exported()
    response = test_client.get(url)
    assert response.status_code == 200


config_content = {
    "servers": {
        "timer": {
            "counters": {
                "Counter": {
                    "test/counter/minutes": {},
                    "test/counter/seconds": {},
                }
            }
        }
    }
}

myobj = {
    "tango_host": "databaseds-tango-base-test",
    "port": "10000",
    "config_content": json.dumps(config_content),
}


def test_configuredb_417():
    url = "/configuredb"
    response = test_client.post(
        url, json=myobj, timeout=1, headers={"x-api-key": apiKey}
    )
    assert response.status_code == 417


def test_unauthorized():
    url = "/configuredb"
    response = test_client.post(
        url, json=myobj, timeout=1, headers={"x-api-key": "wrong"}
    )
    assert response.status_code == 401


def apply_config_mock():
    return [1, 2, 3]


@patch("tango.Database")
@patch("dsconfig.json2tango.apply_config", return_value=(1, 2, 3))
@patch("dsconfig.json2tango.show_output", return_value=0)
def test_configuredb(db, apply_config, show_output):
    # pylint: disable=W0613
    url = "/configuredb"
    response = test_client.post(
        url, json=myobj, timeout=1, headers={"x-api-key": apiKey}
    )
    assert response.status_code == 200
